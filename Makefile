README.md : src/README.md
	pandoc \
		--output $@ \
		--to gfm \
		--filter pandoc-include-filter \
		$^

.PHONY: Presentation.html
Presentation.html : src/Presentation.md
	pandoc \
		--output $@ \
		--to revealjs \
		--standalone \
		--self-contained \
		--variable theme=solarized \
		--variable revealjs-url=reveal.js-3.8.0/ \
		--filter pandoc-include-filter \
		$^
