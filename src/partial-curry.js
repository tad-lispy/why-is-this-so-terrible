"use strict"

const flow = require("lodash/fp/flow")
const curry = require("lodash/fp/curry")


const increment = function (number) { return number + 1 }
const double = function (number) { return number * 2 }
const divideBy = curry(function (a, b) { return b / a })

const compute = flow(
  double,
  increment,
  divideBy(3)
)

console.log(compute(4))
