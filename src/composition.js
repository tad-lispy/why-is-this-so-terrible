"use strict"

const increment = function (number) { return number + 1 }
const double = function (number) { return number * 2 }

const compute = function (number) { 
  return increment(double(number)) 
}

console.log(compute(3))

const flow = require("lodash/fp/flow")

const compute_too = flow(double, increment)

console.log(compute_too(3))
flow(compute_too, console.log)(3)
