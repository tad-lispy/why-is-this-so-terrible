"use strict"

const speaker = {
  name: "Tad Lispy",
  introduce() {
    console.log(`Hello, I'm ${this.name}!`)
  }
}
speaker.introduce()
setTimeout(speaker.introduce, 500)
