"use strict"

const fn = function() { console.log(this) }
const ob = { fn, fatfn:  () => fn() } 

fn(4)
fn.call(3)
fn.apply(2)
fn.bind(1)
ob.fn(0)
ob.fatfn(-1)
