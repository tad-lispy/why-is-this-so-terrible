---
author: Tad Lispy
title: Why is this so Terrible?
subtitle: Elements of Functional Programming for React.js Developers
date: 2020-01-16
---

## 

This presentation is also available at

<https://tad-lispy.gitlab.io/why-is-this-so-terrible/>

<small>
  Source code with most examples:  
  <https://gitlab.com/tad-lispy/why-is-this-so-terrible/tree/master/src>
</small>

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
</a>

<small>
  This work is licensed under a  
  <a 
    rel="license"
    href="http://creativecommons.org/licenses/by-nc-sa/4.0/"
  >
    Creative Commons Attribution - NonCommercial - ShareAlike 4.0 International License
  </a>.
</small>

<small>
  Made with love using [Pandoc][], [RevealJS][] and [Neovim][].  
  Thanks for all the contributors for these awesome tools.
</small>

:::::: notes

Who are the audience?

  - Who is a developer?
  - Who considers themselves a junior?
  - Who is a React developer?
  - Who knows about FP?
  - Who has an opinion?
  - Who thinks it rocks?
  - Who thinks it sucks?

::::::

##

:::::: { .columns }

::: { .column width="50%" height="100%" }
![](./src/tad-lispy.png){ .plain }
:::

::: { .column width="50%" height="100%" }
### Hello, I'm Tad Lispy

  - A software developer,

  - Functional programming coach

    (available for contract)

  - Unix philosopher.

  - [tad-lispy.com](https://tad-lispy.com/)

  - [gitlab.com/tad-lispy](https://gitlab.com/tad-lispy/)

:::

::::::

:::::: notes

Who are the audience?

  - Who is a developer?
  - Who considers themselves a junior?
  - Who is a React developer?
  - Who knows about FP?
  - Who has an opinion?
  - Who thinks it rocks?
  - Who thinks it sucks?

::::::

##

How can you trust your data  
when it has its own mind?


# Essence of Object Oriented Programming

##

The data, state and the computation are coupled together:

``` { .javascript 
      .numberLines 
      include="src/coupled.js"
      startFrom=3
    }
```

## 

Each object is a little actor with its own state. 

It exposes various methods  
via which other actors can communicate with it. 

They are all one big, happy family  
that interacts with one another.

## 

Sounds like a powerful idea.

# The Good

## 

There are some benefits of OOP 

e.g. it is easy to implement stateful computation.

``` { .javascript
      .numberLines
      include="src/stateful.js"
    }
```


# The Bad

## 

But 

> many actors, each with its own state 

sounds complex.

. . . 

And it is!

##

You have to keep track of implicit context

``` { .javascript
      include="src/timeout.js"
      .numberLines
    }
```

##

When you implement a function 

<small>(method is just a function attached to an object)</small>

you don't know what `this` will be.

##

So `this` is like an implicit argument.

It will only be known at the time of calling the function.

``` { .javascript
      include="src/what-is-this.js"
      .numberLines 
    }
```

# The Ugly

##

Even worse, your data starts having brain of its own:

``` { .javascript
      include="src/reduced-to-mockery.js"
      .numberLines 
      startFrom=1
      endAt=3
    }
```

``` { .javascript 
      include="src/reduced-to-mockery.js"
      .numberLines
      .fragment 
      data-fragment-index=2
      startFrom=5
      endAt=5
    }
```

``` { .javascript
      include="src/reduced-to-mockery.js"
      .numberLines 
      startFrom=7
      endAt=7
    }
```

<small class="fragment" data-fragment-index="1">
**Hint**: It will print "Buhahaha 🥳"
</small>

## 

And it may be up to no good

``` { .javascript
      include="src/one-two-three-true.js"
      .numberLines 
      .fragment 
      endAt=6
    }
```

``` { .javascript 
      include="src/one-two-three-true.js"
      .numberLines
      startFrom=8
    }
```

. . .

Seriously, who needs this in their language?

# What's the alternative

## 

Instead of

``` { .javascript .numberLines }
[ 1, 2, 3 ].map((e) => e + 1);
```

why not

``` { .javascript .numberLines }
map((e) => e + 1, [ 1, 2, 3 ])
```

**?**

## 

Or instead of

``` { .javascript .numberLines }
[ 1, 2, 3 ].length;
```

why not

``` { .javascript .numberLines }
length([ 1, 2, 3 ])
```

**?**

# The Essence of Functional Programming

### Composition.

. . .

Things are simple and you can compose them

. . .

A data is just a data: `a`

A function is just a pure function: `a -> b`

Functions don't have state nor side - effects.

##

You can compose data:

``` javascript
const a = 1
const b = 2
const composed = { a, b }
```

##

You can compose functions:

``` { .javascript
      include="src/composition.js"
      .numberLines 
      startFrom=1
      endAt=10
    }
```

## 

Ugly? 

``` { .javascript
      include="src/composition.js"
      .numberLines 
      startFrom=6
      endAt=8
    }
```

. . .

How about this?

``` { .javascript
      include="src/composition.js"
      .numberLines 
      startFrom=12
      endAt=15
    }
```

##

Or even this:

``` { .javascript
      include="src/composition.js"
      .numberLines 
      startFrom=17
    }
```

##

With this setup:

``` { .javascript
      include="src/composition-equivalence.js"
      .numberLines 
      startFrom=7
      endAt=9
    }
```

All of these are equivalent:

``` { .javascript
      include="src/composition-equivalence.js"
      .numberLines 
      startFrom=13
    }
```

## 

But what if the function I want to compose takes some additional arguments?

``` javascript
function divideBy (a, b) { return b / a }
```

##

The lambda way:

<small>*make your self a new function*</small>

``` { .javascript
      include="src/partial-lambda.js"
      .numberLines 
      startFrom=7
    }
```

##

The partial way:

``` { .javascript
      include="src/partial.js"
      .numberLines 
      startFrom=11
    }
```

##

The Indian cuisine way

``` { .javascript
      include="src/partial-curry.js"
      .numberLines 
      startFrom=9
    }
```

. . .

<small>Just kidding. It's a way of [Haskell Brooks Curry][].</small>

##

Using **partial application** and **composition** you can model any computation, no matter how complex.

. . .

And it will almost always be easier to reason about than OOP, because no state means less complexity.

# Simplicity

##

Predictible behaviour, simple rules of composition and partial application.

## 

What follows is that concerns are usually separated

### data | computation | state | effects

You can mix them **if you want to**, but by default they are separated.

# But I need my state!

## 

Sure. Sometimes you do. But ask yourself how often?

##

And if you really do, be explicit about it.

If you are working with React, use [Redux][].

<small>(or whatever else is all the hype this week)</small>

##

If you need effects (e.g. networking), use [Thunks][].

## 

For presentation use functional components.

# Educate Yourself

##

- Read [Professor Frisby's Mostly Adequate Guide to Functional Programming][]

  How to do FP in JS

- Come join us at [Category Theory Study Group][]

  Meet some friendly people and learn the mathematical theory behind this stuff.

[Haskell Brooks Curry]: https://en.wikipedia.org/wiki/Haskell_Curry
[Category Theory Study Group]: https://www.meetup.com/fp-ams/events/267715699/
[Professor Frisby's Mostly Adequate Guide to Functional Programming]: https://mostly-adequate.gitbooks.io/mostly-adequate-guide/
[Redux]: https://redux.js.org/
[Thunks]: https://github.com/reduxjs/redux-thunk
[Pandoc]: https://pandoc.org/
[RevealJS]: https://revealjs.com/
[Neovim]: https://neovim.io/
