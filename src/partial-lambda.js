"use strict"

const flow = require("lodash/fp/flow")

const increment = function (number) { return number + 1 }
const double = function (number) { return number * 2 }
const divideBy = function (a, b) { return b / a }

const compute = flow(
  double,
  increment,
  (number) => divideBy(3, number)
)

console.log(compute(4))
