const stateful = {
  count: 0,
  poke() {
    this.count = this.count + 1
    if (this.count > 3) {
      console.log("**** you!")
    } else {
      console.log("a! ".repeat(this.count))
    }
  }
}

setInterval(() => stateful.poke(), 500)
