"use strict"

// Setup

const flow = require("lodash/fp/flow")

const increment = function (number) { return number + 1 }
const double = function (number) { return number * 2 }
const compute = flow(double, increment)

// All below are equivalent:

console.log(increment(double(3)))
console.log(flow( double, increment )(3))
console.log(compute(3))
flow( double, increment, console.log )(3)
flow( compute, console.log )(3)
